import { makeSqliteDb } from '../src/data-access'
import dotenv from 'dotenv'
dotenv.config();
(async function setupDb () {
  console.log('Setting up database...')
  // database collection will automatically be created if it does not exist
  // indexes will only be added if they don't exist
  const db = await makeSqliteDb()
  console.log(db)
  const sqlExists = `CREATE TABLE IF NOT EXISTS users (id INTEGER PRIMARY KEY NOT NULL,
     first_name TEXT NOT NULL,
     last_name TEXT,
     full_name TEXT,
     email_id CHAR(50) NOT NULL,
     password CAHR(20) NOT NULL,
     created_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
     modified_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP)`;

  await db.run(sqlExists, (err) => {
    console.log('done creating table')
    console.log(err)
  })
  db.close()
  console.log('Database setup complete...')
  process.exit()
})()
