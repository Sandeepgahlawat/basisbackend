export default function makeUsersDb({ makeSqliteDb }) {
  return Object.freeze({
    findById,
    findByEmail,
    insert,
    update,
    updateOtpStatus
  });

  async function updateOtpStatus({ otpStatus, referral, emailId }) {
    const db = await makeSqliteDb();
    const sql = `UPDATE users SET otp_status = ? , referral = ? WHERE email_id = ? `;
    return new Promise((resolve, reject) => {
      db.serialize(() => {
        db.run(sql, [otpStatus, referral, emailId], err => {
          if (err) {
            reject(err);
          }
        });
        db.get(
          `SELECT * FROM users WHERE email_id = ?`,
          [emailId],
          (err, row) => {
            if (err) {
              reject(err);
            }
            console.log("result row: ", row,otpStatus,referral,emailId);
            resolve(row);
          }
        );
      });
    });
  }

  async function findById({ id: _id }) {
    const db = await makeSqliteDb();
    const sql = `SELECT * FROM users WHERE id = ?`;

    return new Promise((resolve, reject) => {
      db.serialize(() => {
        db.get(sql, [_id], (err, row) => {
          if (err) {
            reject(err);
          }
          resolve(row);
        });
      });
    });
  }

  async function insert({
    userName,
    createdOn,
    id,
    password,
    modifiedOn,
    emailId,
    otpStatus,
    firstName,
    lastName
  }) {
    console.log("trying to insert in db");
    console.log(userName);
    const db = await makeSqliteDb();
    const sql = `INSERT INTO users (id, full_name, first_name, last_name, email_id, password,otp_status,created_on,modified_on) VALUES (?,?,?,?,?,?,?,?,?)`;
    const sqlGet = "SELECT * FROM users WHERE id = ?";
    console.log("values are", {
      id,
      userName,
      firstName,
      lastName,
      emailId,
      password,
      otpStatus,
      createdOn,
      modifiedOn
    });

    return new Promise((resolve, reject) => {
      db.serialize(() => {
        db.run(
          sql,
          [
            id,
            userName,
            firstName,
            lastName,
            emailId,
            password,
            otpStatus ? 1 : 0,
            createdOn,
            modifiedOn
          ],
          (err, row) => {
            if (err) {
              console.error("insert db error", err.message);
              reject(err.message);
            }
            console.log("insert result", row);
          }
        );
        db.get(sqlGet, [id], (err, row) => {
          if (err) {
            console.log("get user error", err);
            reject(err);
          }
          console.log("insert result 2 :", row);

          resolve(row);
        });
      });
    });
  }

  async function update({ otp, email }) {
    console.log("checking otp", otp, email);
    const db = await makeSqliteDb();
    const sql = `UPDATE users SET otp = ? WHERE email_id = ?`;
    return new Promise((resolve, reject) => {
      db.serialize(() => {
        db.run(sql, [otp, email], err => {
          if (err) {
            reject(err);
          }
          resolve(true);
        });
      });
    });
  }

  async function findByEmail({ email }) {
    const db = await makeSqliteDb();
    console.log("checking email :", email);
    const sql = "SELECT * FROM users WHERE email_id = ?";
    return new Promise((resolve, reject) => {
      db.serialize(() => {
        db.get(sql, [email], (err, row) => {
          if (err) {
            console.log("find by email err :", err);
            reject(err);
          }
          console.log("result for find by email :", row);
          resolve(row);
        });
      });
    });
  }
}
