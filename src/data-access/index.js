import makeUsersDb from "./users-db";
import sqlite3 from "sqlite3";

const sqlite = sqlite3.verbose();
let db = new sqlite.Database("../../users.db", err => {
  if (err) {
    return console.error(err.message);
  }
  console.log("Connected to the in-memory SQlite database.");
});

export async function makeSqliteDb() {
    console.log('inside making db')
  const sql = `CREATE TABLE IF NOT EXISTS users (
        id TEXT PRIMARY KEY NOT NULL,
        first_name TEXT,
        last_name TEXT,
        full_name TEXT,
        email_id CHAR(50) NOT NULL UNIQUE,
        password CHAR(51) NOT NULL,
        otp CHAR(6),
        otp_status INETGER DEFAULT 0,
        referral TEXT,
        created_on TIMESTAMP DEFAULT current_timestamp,
        modified_on TIMESTAMP DEFAULT current_timestamp )`;
  try {
    const isTableReady = await new Promise((resolve, reject) => {
      db.serialize(() => {
        // db.run("DROP TABLE users");
        db.run(sql, err => {
          if (!err) {
            console.log("table created");
            resolve(true); 
          }
          console.log("table creation error: ", err);
          reject(err);
        });
      });
    });
    return db;
  } catch (e) {
      console.log("create table error: ",e)
    throw new Error("unable to create table");
  }
}

const usersDb = makeUsersDb({ makeSqliteDb });
export default usersDb;
