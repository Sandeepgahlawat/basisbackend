export default function buildMakeUser({ Id, md5, validateEmail, makeSource }) {
  return function makeUser({
    firstName = "",
    lastName = "",
    userName,
    email,
    otpStatus = false,
    createdOn = Date.now(),
    id = Id.makeId(),
    // source,
    modifiedOn = Date.now(),
    password
  } = {}) {
    if (!Id.isValidId(id)) {
      throw new Error("User must have a valid id.");
    }

    if (!email || !validateEmail(email)) {
      throw new Error("User should have a valid email id");
    }

    return Object.freeze({
      getUserName: () => userName,
      getCreatedOn: () => createdOn,
      getId: () => id,
      getModifiedOn: () => modifiedOn,
      getFirstName: () => firstName,
      getLastName: () => lastName,
      // getSource: () => validSource,
      getEmailId: () => email,
      isOtpVarified: () => otpStatus,
      updateOtpStatus: status => {
        otpStatus = status;
      },
      getPassword: () => password,
      setPassword: pass => {
        password = pass;
      }
    });
  };
}
