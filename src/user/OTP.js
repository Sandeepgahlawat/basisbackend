export default function buildMakeOTP({}) {
  return function makeOTP({ otp, referral, emailId } = {}) {
    if (!otp) {
      throw new Error("Otp is not present in request");
    }

    if (!emailId) {
      throw new Error("EmailId is not present");
    }

    return Object.freeze({
      getOtp: () => otp,
      getReferral: () => referral,
      getEmailId: () => emailId,  
    });
  };
}
