import express from "express";
import bodyParser from "body-parser";
import dotenv from "dotenv";
import {
  notFound,
  userSignIn,
  userSignUp,
  passportConfig,
  emailSender,
  verifyOtp
} from "./controllers";
import makeCallback from "./express-callback";
import passport from 'passport'
import cors from 'cors'
// import session from 'express-session'

dotenv.config();

const apiRoot = "";
const app = express();
app.use(bodyParser.json());
// TODO: figure out DNT compliance.
app.use((_, res, next) => {
  res.set({ Tk: "!" });
  next();
});

app.use(cors())

app.use(passport.initialize())

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

app.enable("trust proxy");

passportConfig(passport)
app.post(`${apiRoot}/sign-up`, makeCallback(userSignUp));
app.post(`${apiRoot}/sign-out`, makeCallback());
app.post(`${apiRoot}/sign-in`, makeCallback(userSignIn));
app.post(`${apiRoot}/send-otp`,passport.authenticate('jwt', { session: false }), makeCallback(emailSender))
//
app.get(`${apiRoot}/users/profile`, makeCallback());
app.post(`${apiRoot}/verify-otp`,passport.authenticate('jwt', { session: false }), makeCallback(verifyOtp))
app.use(makeCallback(notFound));

// listen for requests
app.listen(8000, () => {
  console.log("Server is listening on port 8000");
});

export default app;
