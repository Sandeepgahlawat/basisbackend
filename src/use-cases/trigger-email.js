export default function makeTriggerEmail({ usersDb, generateOtp, transporter }) {
  return async function triggerEmail({ email }) {
    // const comment = makeComment(commentInfo)
    const exists = await usersDb.findByEmail({ email });
    if (!exists) {
      return Promise.resolve("User Does not exists");
    }
    const isSent = await usersDb.update({ otp: generateOtp(), email });
    if (isSent) {
      const user = await usersDb.findByEmail({ email });
      const sender = new Promise((resolve, reject) => {
        var mailOptions = {
          from: 'sandeepgahlawat13@gmail.com',
          to: 'sandeepgahlawat12@gmail.com',
          subject: 'Sending Email using Node.js',
          text: `Your OTP for the registration is : ${user.otp}`
        };
        
        transporter.sendMail(mailOptions, (error, info) => {
          if (error) {
            console.log("why this error ",error);
            reject(err)
          } else {
            console.log('Email sent: ' + info.response);
            resolve(user)
          }
        });
      });
      try {
        const user = await sender;
        console.log('finally email sent : ',user)
        return user;
      } catch (e) {
        return e.message;
      }
    }
    return "Unable to update otp in user db";
  };
}
