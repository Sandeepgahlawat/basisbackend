import makeUser from '../user'
export default function makeAddProfile ({ usersDb }) {
  return async function addUserProfile (userInfo) {
    const user = makeUser(userInfo)
    const exists = await usersDb.findByEmail({ emailId: userInfo.emailId })
    if (exists) {
      return exists
    }

    const userSource = user.getSource()
    return usersDb.update({
      firstName: user.getFirstName(),
      lastName: user.getLastName(),
      userName: user.getUserName(),
      createdOn: user.getCreatedOn(),
      id: user.getId(),
      modifiedOn: user.getModifiedOn(),
      source: {
        ip: userSource.getIp(),
        browser: userSource.getBrowser(),
        referrer: userSource.getReferrer()
      },
      emailId: user.getEmailId(),
      otpStatus: user.isOtpVarified()
    })
  }
}
