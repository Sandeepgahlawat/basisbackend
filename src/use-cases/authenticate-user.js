export default function makeAuthenticateUser ({ usersDb }) {
  return async function addUser ( userInfo) {
    const exists = await usersDb.findByEmail({ email: userInfo.email })
    if (exists) {
      console.log(exists)
      return exists
    }
    return "User Does not exist"  
  }
}
