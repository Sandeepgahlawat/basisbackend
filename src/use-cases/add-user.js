import makeUser from "../user";
export default function makeAddUser({ usersDb, bcrypt }) {
  return async function addUser({ httpRequest }) {
    console.log("printing httprequest", httpRequest.body);
    const exists = await usersDb.findByEmail({
      emailId: httpRequest.body.emailId
    });
    if (exists) {
      return exists;
    }
    console.log("checking auth user :",httpRequest);
    const user = makeUser({
      email: httpRequest.body.emailId,
      password: httpRequest.body.password,
      userName: httpRequest.body.userName,
      firstName: httpRequest.body.firstName,
      lastName: httpRequest.body.lastName
    });
    // const userSource = user.getSource();
    const hashedPass = await new Promise((resolve, reject) => {
      bcrypt.genSalt(10, (err, salt) => {
        if (err) reject(err);
        bcrypt.hash(user.getPassword(), salt, (err, hash) => {
          if (err) {
            reject(err);
          }
          resolve(hash);
        });
      });
    });
    console.log("hashed pass", hashedPass);
    user.setPassword(hashedPass);
    console.log("hashed pass 2 :", user.getPassword());
    return usersDb.insert({
      userName: user.getUserName(),
      createdOn: user.getCreatedOn(),
      id: user.getId(),
      password: user.getPassword(),
      modifiedOn: user.getModifiedOn(),
      emailId: user.getEmailId(),
      firstName: user.getFirstName(),
      lastName: user.getLastName(),
      otpStatus: user.isOtpVarified()
    });
  };
}
