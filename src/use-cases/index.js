import makeAddUser from "./add-user";
import makeAddProfile from "./add-profile";
import makeValidateOtp from "./validate-otp";
import makeAuthenticateUser from "./authenticate-user";
import makeTriggerEmail from './trigger-email'
import usersDb from "../data-access";
import bcrypt from 'bcrypt'
import otpGenerator from 'otp-generator'
import nodeMailer from 'nodemailer'

const generateOtp = () => otpGenerator.generate(6, { upperCase: false, specialChars: false });

const transporter = nodeMailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'sandeepgahlawat13@gmail.com',
    pass: '2212199111'
  }
});

const authenticateUser = makeAuthenticateUser({ usersDb });
const addUser = makeAddUser({ usersDb, bcrypt });
const addProfile = makeAddProfile({ usersDb });
const validateOtp = makeValidateOtp({ usersDb });

const triggerEmail = makeTriggerEmail({ usersDb, generateOtp, transporter })

const userService = Object.freeze({
  addUser,
  addProfile,
  validateOtp,
  authenticateUser,
  triggerEmail
});

export default userService;
export {
  addUser,
  addProfile,
  validateOtp,
  authenticateUser,
  triggerEmail
};
