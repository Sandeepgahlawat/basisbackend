export default function makeValidateOtp({ usersDb }) {
  return async function validateOtp({ otp, referral, emailId }) {
    // const user = makeUser(userInfo)
    console.log("inside verify usecase :", { otp, referral, emailId });
    try {
      const exists = await usersDb.findByEmail({ email: emailId });
      if (!exists) {
        return "User Does not exists";
      }
      console.log('comparing otp',exists.otp,otp)
      if (exists.otp === otp) {
        const user = await usersDb.updateOtpStatus({
          otpStatus: exists.otp === otp ? 1 : 0,
          referral,
          emailId
        });
        return user;
      }
      return exists
    } catch (e) {
      console.log(e);
      return e.message
    }
  };
}
