import { Strategy, ExtractJwt } from "passport-jwt";

require("dotenv").config();
const secret = process.env.JWT_SECRET;
// import db
export default function makePassportConfig({ usersDb }) {
  const opts = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: secret
  };
  return async function passportConfig(passport) {
    const headers = {
      "Content-Type": "application/json"
    };

    passport.use(
      new Strategy(opts, async (payload, done) => {
        try {
          console.log("printing from passport config")
          console.log(payload)
          const user = await usersDb.findById({ id: payload.id });
          if (user) {
            payload.user = user
            return done(null, {
              headers,
              statusCode: user == null ? 404 : 200,
              user
            });
          }
          return done(null, false);
        } catch (e) {
          console.error(e);
          return {
            headers,
            statusCode: 400,
            error: e.message
          };
        }
      })
    );
  };
}
