export default function makeVerifyOtp({ validateOtp }) {
  return async function verifyOtp(httpRequest) {
    const headers = {
      "Content-Type": "application/json"
    };

    try {
      console.log("inside verify controller", httpRequest);
      const user = await validateOtp({
        otp: httpRequest.body.passCode,
        referral: httpRequest.body.referral,
        emailId: httpRequest.authUser.email_id
      });
      return {
        headers,
        success: true,
        statusCode: 200,
        body: { user }
      };
    } catch (e) {
      // TODO: Error loggin
      return {
        headers,
        statusCode: 400,
        body: {
          error: e.message
        }
      };
    }
  };
}
