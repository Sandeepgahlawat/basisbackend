export default function makeSendEmail({ triggerEmail }) {
  return async function sendEmail( httpRequest ) {
    const headers = {
      "Content-Type": "application/json"
    };
    try {
      const user = await triggerEmail({ email: httpRequest.body.emailId });
      return {
        headers,
        statusCode: 200,
        body: { user }
      };
    } catch (e) {
      console.log('error in send email: ',e)
      return {
        headers,
        statusCode: 400,
        body: {
          error: e.message
        }
      };
    }
  };
}
