import {
  addUser,
  authenticateUser,
  validateOtp,
  triggerEmail
} from "../use-cases";
import usersDb from "../data-access";
import makeUserSignIn from "./user-sign-in";
import makeUserSignUp from "./user-sign-up";
import makeSendEmail from "./send-email";
import makeVerifyOtp from "./verify-otp";
import notFound from "./not-found";

import makePassportConfig from "./passport-config";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";

const userSignIn = makeUserSignIn({
  authenticateUser,
  bcrypt,
  jwt,
  secret: process.env.JWT_SECRET
});
const userSignUp = makeUserSignUp({
  addUser,
  triggerEmail,
  jwt,
  secret: process.env.JWT_SECRET
});
const emailSender = makeSendEmail({ triggerEmail });
const passportConfig = makePassportConfig({ usersDb });
const verifyOtp = makeVerifyOtp({ validateOtp });
const userController = Object.freeze({
  notFound,
  userSignIn,
  userSignUp,
  passportConfig,
  emailSender,
  verifyOtp
});

export default userController;
export {
  notFound,
  userSignIn,
  userSignUp,
  passportConfig,
  emailSender,
  verifyOtp
};
