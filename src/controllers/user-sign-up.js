export default function makeUserSignUp({ addUser,triggerEmail, jwt, secret }) {
  return async function userSignUp(httpRequest) {
    const headers = {
      "Content-Type": "application/json"
    };
    try {
      const user = await addUser({ httpRequest });

      const payload = {
        id: user.id,
        name: user.userName
      };
      const token = await jwt.sign(payload, secret, { expiresIn: 36000 });

      const result = await triggerEmail({ email: user.email_id });

      if (!token) {
        return {
          headers,
          statusCode: 501,
          body: {
            error: "Unable to generate token"
          }
        };
      }
      result.otpStatus = '0'
      return {
        headers,
        statusCode: user.id == null ? 404 : 200,
        success: true,
        body: { user: result , token: `Bearer ${token}` }
      };
    } catch (e) {
      // TODO: Error logging
      return {
        headers,
        statusCode: 400,
        body: {
          error: e
        }
      };
    }
  };
}
