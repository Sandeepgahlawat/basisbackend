export default function makeUserSignin({
  authenticateUser,
  bcrypt,
  jwt,
  secret
}) {
  return async function userSignIn(httpRequest) {
    const headers = {
      "Content-Type": "application/json"
    };

    try {
      const user = await authenticateUser({
        email: httpRequest.body.emailId,
        password: httpRequest.body.password,
        ...httpRequest
      });
      const isMatch = await bcrypt.compare(
        `${httpRequest.body.password}`,
        user.password
      );
      if (isMatch) {
        const payload = {
          id: user.id,
          name: user.userName
        };
        const token = await jwt.sign(payload, secret, { expiresIn: 36000 });
       
        if (!token) {
          return {
            headers,
            success: true,
            statusCode: 501,
            body: { error: "Error signing token" }
          };
        }
        return {
          headers,
          success: true,
          statusCode: 200,
          body: { user, token: `Bearer ${token}` }
        };
      } else {
        return {
          headers,
          statusCode: 200,
          body: { error: "Password did not match" }
        };
      }
    } catch (e) {
      // TODO: Error logging
      console.log("error in match : ", e);
      return {
        headers,
        statusCode: 400,
        body: {
          error: e.message
        }
      };
    }
  };
}
